# CxxProf
CxxProf is a manual instrumented Profiling library for C++.

## Wiki
Please take a look at the Wiki to get information about the following topics:

* [What is CxxProf?](https://gitlab.com/CxxProf/Documentation/wikis/What-is-CxxProf%3F)
* [Screenshots and Examples](https://gitlab.com/CxxProf/Documentation/wikis/Screenshots-and-Examples)
* [Architecture Overview](https://gitlab.com/CxxProf/Documentation/wikis/Architecture-Overview)
* [Usage: Integrate CxxProf into your project](https://gitlab.com/CxxProf/Documentation/wikis/Usage:-Integrate-CxxProf-into-your-project)
* [Usage: A coding tutorial](https://gitlab.com/CxxProf/Documentation/wikis/Usage:-A-coding-tutorial)
* [Usage: Using the Chromium Tracing interface](https://gitlab.com/CxxProf/Documentation/wikis/Using-the-Chromium-Tracing-interface)
* [DevGuide: How to compile CxxProf](https://gitlab.com/CxxProf/Documentation/wikis/DevGuide:-How-to-compile-CxxProf)
* [List of existing Profilers (References)](https://gitlab.com/CxxProf/Documentation/wikis/List-of-existing-Profilers-(References))
* [Useful Links](https://gitlab.com/CxxProf/Documentation/wikis/Useful-Links)

## Copying
Free use of this software is granted under the terms of the GNU Lesser General Public License (LGPL). For details see the files `COPYING` and `COPYING.LESSER` included with the CxxProf distribution.
